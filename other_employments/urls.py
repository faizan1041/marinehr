from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^other_employments/$', views.OtherEmploymentsList.as_view()),
    url(r'^other_employments/(?P<pk>[0-9]+)/$', views.OtherEmploymentsDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
