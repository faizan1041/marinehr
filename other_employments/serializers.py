from rest_framework import serializers
from models import OtherEmployments
from django.contrib.auth.models import User


class OtherEmploymentsSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = OtherEmployments
        fields = tuple([x.name for x in OtherEmployments._meta.get_fields(include_hidden=True)])
