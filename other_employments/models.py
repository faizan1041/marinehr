from django.contrib.auth.models import User
from django.db import models


class OtherEmployments(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    from_date = models.DateField()
    to_date = models.DateField()


    charfields = ['country','title','company']

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(OtherEmployments, self).save(*args, **kwargs)


    class Meta:
        db_table = 'other_employments'
