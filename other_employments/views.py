from models import OtherEmployments
from serializers import OtherEmploymentsSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class OtherEmploymentsList(generics.ListCreateAPIView):
    queryset = OtherEmployments.objects.all()
    serializer_class = OtherEmploymentsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class OtherEmploymentsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = OtherEmployments.objects.all()
    serializer_class = OtherEmploymentsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
