from rest_framework import serializers
from models import Passport
from django.contrib.auth.models import User


class PassportSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Passport
        fields = tuple([x.name for x in Passport._meta.get_fields(include_hidden=True)])
