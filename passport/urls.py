from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^passport/$', views.PassportList.as_view()),
    url(r'^passport/(?P<pk>[0-9]+)/$', views.PassportDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
