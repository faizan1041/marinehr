from django.db import models
from django.contrib.auth.models import User



class Passport(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_issued = models.DateField()
    valid_until = models.DateField()


    charfields = ['nationality', 'number', 'place_issued']

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(Passport, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created',)
        db_table = 'passport'
