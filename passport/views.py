from models import Passport
from serializers import PassportSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class PassportList(generics.ListCreateAPIView):
    queryset = Passport.objects.all()
    serializer_class = PassportSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class PassportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Passport.objects.all()
    serializer_class = PassportSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
