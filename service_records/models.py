from django.contrib.auth.models import User
from django.db import models


class ServiceRecords(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    from_date = models.DateField()
    to_date = models.DateField()


    charfields = ['vessel_name','ship_type','rank','department','company']

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(ServiceRecords, self).save(*args, **kwargs)


    class Meta:
        db_table = 'service_records'
