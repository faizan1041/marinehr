from models import ServiceRecords
from serializers import ServiceRecordsSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class ServiceRecordsList(generics.ListCreateAPIView):
    queryset = ServiceRecords.objects.all()
    serializer_class = ServiceRecordsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class ServiceRecordsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ServiceRecords.objects.all()
    serializer_class = ServiceRecordsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
