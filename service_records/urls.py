from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^service_records/$', views.ServiceRecordsList.as_view()),
    url(r'^service_records/(?P<pk>[0-9]+)/$', views.ServiceRecordsDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
