from rest_framework import serializers
from models import ServiceRecords
from django.contrib.auth.models import User


class ServiceRecordsSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = ServiceRecords
        fields = tuple([x.name for x in ServiceRecords._meta.get_fields(include_hidden=True)])
