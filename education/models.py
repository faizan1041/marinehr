from django.db import models
from django.contrib.auth.models import User



class Education(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    

    charfields = [
                  'level', 'institute', 'country'
                  ]

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(Education, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created',)
        db_table = 'education'
