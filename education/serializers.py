from rest_framework import serializers
from models import Education
from django.contrib.auth.models import User


class EducationSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Education
        fields = tuple([x.name for x in Education._meta.get_fields(include_hidden=True)])
