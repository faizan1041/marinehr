from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^education/$', views.EducationList.as_view()),
    url(r'^education/(?P<pk>[0-9]+)/$', views.EducationDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
