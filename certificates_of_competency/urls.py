from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^certificates_of_competency/$', views.CertificatesOfCompetencyList.as_view()),
    url(r'^certificates_of_competency/(?P<pk>[0-9]+)/$', views.CertificatesOfCompetencyDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
