from models import CertificatesOfCompetency
from serializers import CertificatesOfCompetencySerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class CertificatesOfCompetencyList(generics.ListCreateAPIView):
    queryset = CertificatesOfCompetency.objects.all()
    serializer_class = CertificatesOfCompetencySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class CertificatesOfCompetencyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CertificatesOfCompetency.objects.all()
    serializer_class = CertificatesOfCompetencySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
