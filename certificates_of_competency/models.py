from django.contrib.auth.models import User
from django.db import models


class CertificatesOfCompetency(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    valid_until = models.DateField()


    charfields = ['type', 'number', 'certificate']

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(CertificatesOfCompetency, self).save(*args, **kwargs)


    class Meta:
        db_table = 'certificates_of_competency'
