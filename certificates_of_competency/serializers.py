from rest_framework import serializers
from models import CertificatesOfCompetency
from django.contrib.auth.models import User


class CertificatesOfCompetencySerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = CertificatesOfCompetency
        fields = tuple([x.name for x in CertificatesOfCompetency._meta.get_fields(include_hidden=True)])
