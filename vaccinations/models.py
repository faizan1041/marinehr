from django.contrib.auth.models import User
from django.db import models


class Vaccinations(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_issued = models.DateField()
    valid_until = models.DateField()


    charfields = ['document', 'place_issued']

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(Vaccinations, self).save(*args, **kwargs)


    class Meta:
        db_table = 'vaccinations'
