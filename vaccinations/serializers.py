from rest_framework import serializers
from models import Vaccinations
from django.contrib.auth.models import User


class VaccinationsSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Vaccinations
        fields = tuple([x.name for x in Vaccinations._meta.get_fields(include_hidden=True)])
