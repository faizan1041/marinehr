from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^vaccinations/$', views.VaccinationsList.as_view()),
    url(r'^vaccinations/(?P<pk>[0-9]+)/$', views.VaccinationsDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
