from models import Vaccinations
from serializers import VaccinationsSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class VaccinationsList(generics.ListCreateAPIView):
    queryset = Vaccinations.objects.all()
    serializer_class = VaccinationsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class VaccinationsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vaccinations.objects.all()
    serializer_class = VaccinationsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
