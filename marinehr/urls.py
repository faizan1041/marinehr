from django.conf.urls import url, include
from rest_framework import routers
import marinehr.views as views
from django.contrib import admin


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('accounts.urls', namespace="accounts")),
    url(r'^api/', include('personal.urls')),
    url(r'^api/', include('passport.urls')),
    url(r'^api/', include('medical_exam.urls')),
    url(r'^api/', include('seaman_book.urls')),
    url(r'^api/', include('education.urls')),
    url(r'^api/', include('vaccinations.urls')),
    url(r'^api/', include('certificates.urls')),
    url(r'^api/', include('certificates_of_competency.urls')),
    url(r'^api/', include('current_employment.urls')),
    url(r'^api/', include('service_records.urls')),
    url(r'^api/', include('other_employments.urls')),
]
