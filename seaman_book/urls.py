from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^seaman_book/$', views.SeamanBookList.as_view()),
    url(r'^seaman_book/(?P<pk>[0-9]+)/$', views.SeamanBookDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
