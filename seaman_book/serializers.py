from rest_framework import serializers
from models import SeamanBook
from django.contrib.auth.models import User


class SeamanBookSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = SeamanBook
        fields = tuple([x.name for x in SeamanBook._meta.get_fields(include_hidden=True)])
