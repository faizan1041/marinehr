from models import SeamanBook
from serializers import SeamanBookSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class SeamanBookList(generics.ListCreateAPIView):
    queryset = SeamanBook.objects.all()
    serializer_class = SeamanBookSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class SeamanBookDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SeamanBook.objects.all()
    serializer_class = SeamanBookSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
