from rest_framework import serializers
from models import Certificates
from django.contrib.auth.models import User


class CertificatesSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Certificates
        fields = tuple([x.name for x in Certificates._meta.get_fields(include_hidden=True)])
