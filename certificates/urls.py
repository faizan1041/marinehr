from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^certificates/$', views.CertificatesList.as_view()),
    url(r'^certificates/(?P<pk>[0-9]+)/$', views.CertificatesDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
