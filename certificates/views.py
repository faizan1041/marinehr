from models import Certificates
from serializers import CertificatesSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class CertificatesList(generics.ListCreateAPIView):
    queryset = Certificates.objects.all()
    serializer_class = CertificatesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class CertificatesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Certificates.objects.all()
    serializer_class = CertificatesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
