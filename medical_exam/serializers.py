from rest_framework import serializers
from models import MedicalExam
from django.contrib.auth.models import User


class MedicalExamSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = MedicalExam
        fields = tuple([x.name for x in MedicalExam._meta.get_fields(include_hidden=True)])
