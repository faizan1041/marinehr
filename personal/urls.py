from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^personal/$', views.PersonalList.as_view()),
    url(r'^personal/(?P<pk>[0-9]+)/$', views.PersonalDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
