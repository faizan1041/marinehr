from django.db import models
from django.contrib.auth.models import User



class Personal(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    seafarer_id = models.CharField(max_length=30, blank=True, unique=True)


    charfields = [
                  'location', 'department', 'rank',
                  'desired_annual_salary', 'desired_contract_type',
                  'desired_contract_dynamics', 'years_of_experience',
                  'availability_date', 'marital_status',
                  ]

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(Personal, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created',)
        db_table = 'personal'
