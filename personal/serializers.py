from rest_framework import serializers
from models import Personal
from django.contrib.auth.models import User


class PersonalSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Personal
        fields = tuple([x.name for x in Personal._meta.get_fields(include_hidden=True)])
