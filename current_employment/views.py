from models import CurrentEmployment
from serializers import CurrentEmploymentSerializer
from rest_framework import generics
from rest_framework import permissions
from permissions import IsOwnerOrReadOnly
import pandas as pd




class CurrentEmploymentList(generics.ListCreateAPIView):
    queryset = CurrentEmployment.objects.all()
    serializer_class = CurrentEmploymentSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)




class CurrentEmploymentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CurrentEmployment.objects.all()
    serializer_class = CurrentEmploymentSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
