from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = [
    url(r'^current_employment/$', views.CurrentEmploymentList.as_view()),
    url(r'^current_employment/(?P<pk>[0-9]+)/$', views.CurrentEmploymentDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
