from rest_framework import serializers
from models import CurrentEmployment
from django.contrib.auth.models import User


class CurrentEmploymentSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = CurrentEmployment
        fields = tuple([x.name for x in CurrentEmployment._meta.get_fields(include_hidden=True)])
